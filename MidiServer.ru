#!/usr/bin/env ruby
# encoding: utf-8

require 'drb/drb'
require 'micromidi'

# The URI we're using for RDb, we use unix sockets just because they're quicker.
DRB_URI="drbunix:/var/tmp/sonic-pi-midiconnector"

class SPIMidiConnector

  def initialize
    @input = UniMIDI::Input.use(0)
    @midithread = listen()
  end

  # Return the value of the pot.
  #
  # If we haven't moved a pot yet then just return 0.
  def get(key)
    @midithread[key] || 0
  end

def k1; get(:k1); end
def k2; get(:k2); end
def k3; get(:k3); end
def k4; get(:k4); end
def k5; get(:k5); end
def k6; get(:k6); end
def k7; get(:k7); end
def k8; get(:k8); end
def k9; get(:k9); end
def k10; get(:k10); end
def k11; get(:k11); end
def k12; get(:k12); end
def k13; get(:k13); end
def k14; get(:k14); end
def k15; get(:k15); end
def k16; get(:k16); end
def k17; get(:k17); end
def k18; get(:k18); end
def k19; get(:k19); end
def k20; get(:k20); end
def k21; get(:k21); end
def k22; get(:k22); end
def k23; get(:k23); end
def k24; get(:k24); end
def k25; get(:k25); end
def k26; get(:k26); end
def k27; get(:k27); end
def k28; get(:k28); end
def k29; get(:k29); end
def k30; get(:k30); end
def k31; get(:k31); end
def k32; get(:k32); end
def k33; get(:k33); end
def k34; get(:k34); end
def k35; get(:k35); end
def k36; get(:k36); end
def k37; get(:k37); end
def k38; get(:k38); end
def k39; get(:k39); end
def k40; get(:k40); end
def k41; get(:k41); end
def k42; get(:k42); end
def k43; get(:k43); end
def k44; get(:k44); end
 
  def thread
    @midithread
  end

private

  def listen
    midithread = Thread.new(@pads_map) do |pad_map|
      MIDI.using(@input) do

        # When we get a control change message, update a
        # thread attribute with the value.
        thru_except :control_change do |msg|
          key = "k#{msg.data[0]}".to_sym
          puts "key: #{key} value: #{msg.value}"
          midithread[key] = msg.value
        end

        join
      end
    end
    midithread
  end
end

# Then we start the DRb server, as per the DRb docs.

FRONT_OBJECT = SPIMidiConnector.new

$SAFE = 1

DRb.start_service(DRB_URI, FRONT_OBJECT)
DRb.thread.join
