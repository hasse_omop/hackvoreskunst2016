# README #

Just a brief intro to get the example running

### Install ###

* Ruby
* Sonic pi
* A local webserver (apache)
* PHP
* Mysql

### How do you get set up? ###

* Import Database.sql to mysql
* Setup your webserver and php. Place data.php in your webroot and edit the connection settings in the script. 
* Attach a midi controller to the computer
* Run MidiServer.ru with Ruby and check you get an input from the midi controller
* Open Sonic Pi and load Audio.ru (Copy-paste the code)
* Play the tune and change the values from the midi keyboard
* And then: bit.ly/HackVoresKunst  ;)