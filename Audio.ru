require 'drb/drb'
require 'open-uri'

DRB_URI="drbunix:/var/tmp/sonic-pi-midiconnector"
@lpd = DRbObject.new_with_uri(DRB_URI)

$c1 = "0"
$c2 = "0"
$c3 = "0"
$c4 = "0"

$m1 = "0"
$m2 = "0"
$m3 = "0"
$m4 = "0"

$cm1
$cm2
$cm3
$cm4

$ampli_a = 0
$panning_a = 0
$ampli_o = 0
$panning_o = 0
$photo = 0

$m1_max = 70
$m2_max = 10

in_thread do
  loop do
    $ampli_a = 5/($m1_max-ReadAndMapMidi1()+0.01)
    $panning_a = ($m1_max-ReadAndMapMidi1())/$m1_max
    $ampli_o = 5/(($m2_max-ReadAndMapMidi2())+0.01)
    $panning_o = -(($m2_max-ReadAndMapMidi2())/$m2_max)
    $photo = true #ReadAndMapMidi3()
  sleep 1
  end
end

in_thread do
  loop do
    cue :tick
    sleep 1
  end
end


in_thread do
  if $photo
    1.times do
      sample :ambi_dark_woosh, attack: 1, attack_level: 2, decay: 0.5, decay_level: 1, sustain: 1, release: 2
      sleep 4
      sample :ambi_dark_woosh
      sleep 2
    end
  else
  end
end
# akryl
in_thread do
  loop do
    sync :tick
    with_fx :reverb do
      with_fx :flanger do
        with_fx :echo, phase: rand, decay: rrand_i(2,10), amp: $ampli_a do
          use_synth :hollow
          play 70, pan: $panning_a, amp: $ampli_a
          sleep 0.5
          sample :ambi_piano, pan: $panning_a, amp: $ampli_a
          sleep 0.5
          sample :misc_burp, pan: $panning_a, amp: $ampli_a
          sleep 0.5
        end
      end
    end
  end
end
in_thread do
  use_synth :tb303
  loop do
    sync :tick
    play 50, release: 0.1, cutoff: rrand(60, 120), pan: $panning_a, amp: $ampli_a
    sleep 0.125
  end
end
in_thread do
  loop do
    sync :tick
    with_fx :gverb do
      sample :drum_bass_hard, pan: $panning_a, amp: $ampli_a
      sleep 0.75
    end
  end
end
in_thread do
  loop do
    sync :tick
    with_fx :bitcrusher do
      use_synth :dpulse
      loop do
        play 30, release: 0.5, cutoff: rrand(60, 120), pan: $panning_a, amp: $ampli_a
        sleep 0.125
      end
    end
  end
end
in_thread do
  loop do
    sync :tick
    with_fx :reverb do
      with_fx :echo, mix: 1, phase: 0.8 do
        sample :ambi_choir, pan: $panning_a, attack: 0.5, sustain: 3, decay: rrand_i(5,7), rate: 0.4
        sleep 8
      end
    end
  end
end

# olie

in_thread do
  loop do
    sync :tick
    with_fx :reverb do
      with_fx :wobble do
        with_fx :echo, phase: rand, decay: rrand_i(2,10), amp: $ampli_o do
          use_synth :growl
          play 50, pan: $panning_o, amp: $ampli_o
          sleep 0.5
          sample :elec_plip, pan: $panning_o, amp: $ampli_o
          sleep 0.5
          sample :drum_bass_soft, pan: $panning_o, amp: $ampli_o
          sleep 0.5
        end
      end
    end
  end
end
in_thread do
  use_synth :pnoise
  loop do
    sync :tick
    play 50, release: 0.1, cutoff: rrand(60, 120), pan: $panning_o, amp: $ampli_o
    sleep 0.125
  end
end
in_thread do
  loop do
    sync :tick
    with_fx :flanger, room: 0.5 do
      use_synth :pretty_bell
      play 30, pan: $panning_o, amp: $ampli_o, attack: 1, decay: 1
      sleep 4
    end
  end
end
in_thread do
  loop do
    sync :tick
    with_fx :hpf do
      use_synth :pnoise
      loop do
        play 50, release: 0.1, cutoff: rrand(60, 120), pan: $panning_o, amp: $ampli_o
        sleep 0.125
      end
    end
  end
end

def ReadAndMapMidi1()
  m = "olie"
  print "c1 label: " +m
  print "c1 max: "+$m1.to_s
  value = @lpd.k2
  value = value + 1918
  value = value.to_s

  if $cm1.to_s == value
    print "c1 midi: "+$cm1.to_s
    print "c1 value: "+$c1.to_s
    return $c1.to_i
  else

    $cm1 = value

    url = "http://localhost/data.php?medium="+m+"&year="+value
    print url
    source = open(url){|f|f.read}
    $c1 = source
    if $c1 > $m1
      $m1 = $c1
    end

    print "c1 midi: "+$cm1.to_s
    print "c1 value: "+$c1.to_s
    return source.to_i
  end

end

def ReadAndMapMidi2()
  m = "akryl"
  print "c2 label: " +m
  print "c2 max: "+$m2.to_s
  value = @lpd.k3
  value = value + 1918
  value = value.to_s

  if $cm2.to_s == value
    print "c2 midi: "+$cm2.to_s
    print "c2 value: "+$c2.to_s
    return $c2.to_i
  else

    $cm2 = value

    url = "http://localhost/data.php?medium="+m+"&year="+value
    print url
    source = open(url){|f|f.read}
    $c2 = source
    if $c2 > $m2
      $m2 = $c2
    end

    print "c2 midi: "+$cm2.to_s
    print "c2 value: "+$c2.to_s
    return source.to_i
  end

end
